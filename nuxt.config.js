export default {
  mode: 'spa',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module'
    '@nuxtjs/dotenv'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/toast'
  ],
  router: {
    middleware: 'forceHttps'
  },
  toast: {
    position: 'top-center',
    duration: 3000
  },
  env: {
    baseUrl: 'https://qismo.qiscus.com/api/v1/',
    // baseUrl: 'https://qismo-stag.qiscus.com/api/v1/',
    //ruang guru
    appId: 'ump-jks0xieqivs8uasfu',
    //qismo tester
    // appId: 'opu-9o2i1y1ghvxnuoygh',
    //qismo tester stag
    // appId: 'fuel-wdiiuy457lzmexbb',
    //integration team
    // appId: 'karm-gzu41e4e4dv9fu3f',
    baseUrlAPI: 'https://rg-rooms-intermediary.herokuapp.com/'
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
