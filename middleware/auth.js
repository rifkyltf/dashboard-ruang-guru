export default function({ store, redirect }) {
  const auth = localStorage.getItem('auth')
  if (auth == null) return redirect('/login')
}
