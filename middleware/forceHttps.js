export default function({ store, redirect }) {
    let host = location.hostname
    if (host != 'localhost' && host != '127.0.0.1') {
        if (location.protocol != 'https:') {
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
    }
}
  